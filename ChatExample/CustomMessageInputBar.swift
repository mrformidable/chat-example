//
//  CustomMessageInputBar.swift
//  ChatExample
//
//  Created by Michael Aidoo on 2018-07-03.
//  Copyright © 2018 Michael Aidoo. All rights reserved.
//

import Foundation
import MessageKit

class CustomMessageInputBar: MessageInputBar {
    var photoButtonAction: (() -> ())?

    private lazy var pictureButton: InputBarButtonItem = {
        return InputBarButtonItem().configure {
            $0.spacing = .flexible
            $0.image = UIImage(named: "pic_icon")?.withRenderingMode(.alwaysOriginal)
            $0.setSize(CGSize(width: 25, height: 25), animated: true)
            }.onSelected {
                $0.tintColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
            }.onDeselected {
                $0.tintColor = UIColor.lightGray
            }.onTouchUpInside { [weak self] _ in
                self?.photoButtonAction?()
        }
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func setup() {
        super.setup()
        separatorLine.isHidden = true
        isTranslucent = true
        backgroundView.backgroundColor = .white
        backgroundView.alpha = 0.95
        
        inputTextView.layer.borderColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1).cgColor
        inputTextView.layer.borderWidth = 1.0
        inputTextView.layer.cornerRadius = 17.0
        inputTextView.layer.masksToBounds = true
        
        inputTextView.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
        inputTextView.textContainerInset = UIEdgeInsets(top: 8, left: 10, bottom: 8, right: 36)
        inputTextView.placeholderLabelInsets = UIEdgeInsets(top: 9, left: 15, bottom: 8, right: 36)
        inputTextView.placeholderLabel.font = UIFont.systemFont(ofSize: 16)
        textViewPadding.right = -35
        textViewPadding.left = 8
        
        setLeftStackViewWidthConstant(to: 25, animated: false)
        setStackViewItems([pictureButton], forStack: .left, animated: false)
        pictureButton.contentEdgeInsets = UIEdgeInsets(top: -7, left: 0, bottom: 7, right: 0)
        
        setRightStackViewWidthConstant(to: 28, animated: false)
        setStackViewItems([sendButton], forStack: .right, animated: false)
        
        sendButton.title = nil
        sendButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 4, right: 0)
        sendButton.image = UIImage(named: "send_icon")
        sendButton.setSize(CGSize(width: 28, height: 28), animated: false)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
