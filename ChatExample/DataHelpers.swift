//
//  DataHelpers.swift
//  ChatExample
//
//  Created by Michael Aidoo on 2018-07-03.
//  Copyright © 2018 Michael Aidoo. All rights reserved.
//

import Foundation
import MessageKit

struct User {
    let id: String
    let email: String
    let profileImageUrl: String?
}

struct Message: MessageType {
    var sender: Sender
    var messageId: String
    var sentDate: Date
    var kind: MessageKind
}

struct FakeMessages {
    static let shared = FakeMessages()
    private init() {}
    
    @discardableResult
    func generate() -> [Message] {
        
        var messages: [Message] = []
        let message1 = Message(sender: sender(), messageId: UUID().uuidString, sentDate: Date(), kind: .text("This is a fake sample message to illustrate message from the recepient side"))
        let message2 = Message(sender: sender(), messageId: UUID().uuidString, sentDate: Date(), kind: .text("This should work"))
        let message3 = Message(sender: sender(), messageId: UUID().uuidString, sentDate: Date(), kind: .text("This is a fake sample message to illustrate message from the recepient side This is a fake sample message to illustrate message from the recepient side This is a fake sample message to illustrate message from the recepient side"))
        let mediaItem = MessageMediaItem(image: UIImage(named: "tower")!)
        
        let _ = Message(sender: sender(), messageId: UUID().uuidString, sentDate: Date(), kind: .photo(mediaItem))
        
        messages.append(message1)
        messages.append(message2)
        messages.append(message3)
        //messages.append(message4)
        return messages
    }
    
    private func sender() -> Sender {
        let john = Sender(id: "john123", displayName: "John")
        return john
    }
}

