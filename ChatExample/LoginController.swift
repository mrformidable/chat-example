//
//  LoginController.swift
//  ChatExample
//
//  Created by Michael Aidoo on 2018-07-04.
//  Copyright © 2018 Michael Aidoo. All rights reserved.
//

import Foundation
import UIKit

class LoginController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        FirebaseAccessManager.shared.loginUser(withEmail: email, password: password) {  success in
            if success {
                self.performSegue(withIdentifier: "ShowChat", sender: nil)
            }
        }
    }
    
    
}
