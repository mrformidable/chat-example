//
//  MessageMediaItem.swift
//  ChatExample
//
//  Created by Michael Aidoo on 2018-07-03.
//  Copyright © 2018 Michael Aidoo. All rights reserved.
//

import Foundation
import MessageKit

struct MessageMediaItem: MediaItem {
    var url: URL?
    var image: UIImage?
    var placeholderImage: UIImage
    var size: CGSize
    
    init(image: UIImage) {
        self.image = image
        self.size = CGSize(width: 240, height: 240)
        self.placeholderImage = UIImage()
    }
}
