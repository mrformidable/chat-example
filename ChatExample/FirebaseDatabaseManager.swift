//
//  FirebaseDatabaseManager.swift
//  ChatExample
//
//  Created by Michael Aidoo on 2018-07-04.
//  Copyright © 2018 Michael Aidoo. All rights reserved.
//

import UIKit
import Foundation
import Firebase
import FirebaseAuth
import FirebaseStorage

class FirebaseDatabaseManager: MessageDatabseWriteable {
    
    private init() {}
    static let shared = FirebaseDatabaseManager()
    
    func saveMessage(_ message: MessageData) {
        
    }
    
    func createUser(_ email: String, password: String, profileImage: UIImage, completion: @escaping (Bool) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if error != nil {
                print(error!.localizedDescription)
                completion(false)
                return
            }
            guard let result = result else {
                completion(false)
                return
            }
            
            let userId = result.user.uid
            let profileImageData = profileImage.jpegData(compressionQuality: 0.5)!
            self.saveUser(with: userId, email: email, imageData: profileImageData)
            completion(true)
        }
    }
    
    func saveUser(with id: String, email: String, imageData: Data) {
        let imageName = UUID().uuidString
        let storage = Storage.storage().reference().child("profile_images").child("\(imageName).jpg")
        
        storage.putData(imageData, metadata: nil) { (metadata, error) in
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            guard let profileImageUrl = metadata?.path else { return }
            
            let values: [String: Any] = ["email": email, "profileImageUrl": profileImageUrl]
            Database.database().reference().child("users").child(id).updateChildValues(values, withCompletionBlock: { (error, ref) in
                if error != nil {
                    print(error!.localizedDescription)
                    return
                }
            })
        }
    }
}

class FirebaseAccessManager: DatabaseAccessProtocol {
    
    private init() {}
    static let shared = FirebaseAccessManager()
    
    func loginUser(withEmail email: String, password: String, completion: @escaping (Bool) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if error != nil {
                print(error!.localizedDescription)
                completion(false)
                return
            }
            completion(true)
        }
    }
    
    func logoutUser() {
        try? Auth.auth().signOut()
    }
    
}
