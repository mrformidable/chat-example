//
//  Database.swift
//  ChatExample
//
//  Created by Michael Aidoo on 2018-07-04.
//  Copyright © 2018 Michael Aidoo. All rights reserved.
//

import Foundation

protocol MessageDatabseWriteable {
    func saveMessage(_ message: MessageData) 
    func saveUser(with id: String, email: String, imageData: Data)
}

protocol MessageDatabaseReadable {
    func loadMessages()
}

protocol MessageData {
    var messageId: String { get }
    var senderId: String { get }
    var date: Date { get }
}

protocol DatabaseAccessProtocol {
    func loginUser(withEmail email: String, password: String, completion: @escaping (Bool) -> Void)
    func logoutUser()
}
