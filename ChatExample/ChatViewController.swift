//
//  ChatViewController.swift
//  ChatExample
//
//  Created by Michael Aidoo on 2018-07-02.
//  Copyright © 2018 Michael Aidoo. All rights reserved.
//

import UIKit
import Foundation
import MessageKit

class ChatViewController: MessagesViewController {
    
    let defaultGreen = UIColor(red: 3/255, green: 208/255, blue: 72/255, alpha: 1)
    let defaultBlue = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
    lazy var formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        return formatter
    }()
    
    private let customInputBar = CustomMessageInputBar()
    
    var messages: [MessageType] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        
        scrollsToBottomOnKeybordBeginsEditing = true
        maintainPositionOnKeyboardFrameChanged = true
        
        setupInputBar()
        handleTappingPhotoButton()
    }
    
    private func setupInputBar() {
        customInputBar.delegate = self
        messageInputBar = customInputBar
        loadFakeMessages()
    }
    
    private func handleTappingPhotoButton() {
        customInputBar.photoButtonAction = { [weak self] in
            self?.showPhotoLibrary()
        }
    }
    
    private func showPhotoLibrary() {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = true
        self.present(pickerController, animated: true, completion: nil)
    }
    
    private func loadFakeMessages() {
        self.messages = FakeMessages.shared.generate()
        messagesCollectionView.reloadData()
    }
    
    deinit {
        print("view is deinitializing")
    }
    
    func dateComponents(for date: Date) -> DateComponents {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .weekOfMonth, .weekOfYear, .weekdayOrdinal, .yearForWeekOfYear], from: date)
        return components
    }
    
}

extension ChatViewController: MessagesDataSource {
    func currentSender() -> Sender {
        return Sender(id: "mike123", displayName: "Michael")
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messages[indexPath.section]
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
    
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        if indexPath.section % 3 == 0 {
            return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
        return nil
    }
    
    func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let dateString = formatter.string(from: message.sentDate)
        return NSAttributedString(string: dateString, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption2)])
    }
    
}

extension ChatViewController: MessagesLayoutDelegate {
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .darkText : .darkText
    }
    
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .white : UIColor(red: 227/255, green: 227/255, blue: 231/255, alpha: 1)
    }
    
    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        let color: UIColor = isFromCurrentSender(message: message) ? UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1) : .clear
        let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        let tail: MessageStyle.TailStyle = isFromCurrentSender(message: message) ? .pointedEdge : .pointedEdge
        return MessageStyle.bubbleTailOutline(color, corner, tail)
    }
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        if isFromCurrentSender(message: message) {
            avatarView.isHidden = true
            if let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout {
                layout.textMessageSizeCalculator.outgoingAvatarSize = .zero
                layout.setMessageOutgoingAvatarSize(.zero)
                layout.setMessageOutgoingMessageBottomLabelAlignment(.init(textAlignment: .right, textInsets: UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)))
                layout.setMessageOutgoingMessageTopLabelAlignment(.init(textAlignment: .right, textInsets: UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)))
            }
            
        } else {
            avatarView.isHidden = false
            avatarView.set(avatar: Avatar(image: nil, initials: "MA"))
            avatarView.backgroundColor = UIColor(red: 150 / 255, green: 155 / 255, blue: 166 / 255, alpha: 1)
        }
    }
}

extension ChatViewController: MessagesDisplayDelegate {
    func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        if indexPath.section % 3 == 0 {
            return 10
        }
        return 0
    }
    
    func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 10
    }
    
    func messageBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 16
    }
}

extension ChatViewController: MessageInputBarDelegate {
    func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        for component in inputBar.inputTextView.components {
            if let image = component as? UIImage {
                let mediaItem = MessageMediaItem(image: image)
                let message = Message(sender: currentSender(), messageId: UUID().uuidString, sentDate: Date(), kind: .photo(mediaItem))
                messages.append(message)
                messagesCollectionView.insertSections([messages.count - 1])
            }
            
            if let text = component as? String {
                let message = Message(sender: currentSender(), messageId: UUID().uuidString, sentDate: Date(), kind: .text(text))
                messages.append(message)
                messagesCollectionView.insertSections([messages.count - 1])
            }
        }
        
        inputBar.inputTextView.text = ""
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.messagesCollectionView.scrollToBottom(animated: true)
        }
    }
    
    func messageInputBar(_ inputBar: MessageInputBar, textViewTextDidChangeTo text: String) {
        print(inputBar)
    }
    
    func messageInputBar(_ inputBar: MessageInputBar, didChangeIntrinsicContentTo size: CGSize) {
        print(inputBar)
    }
    
}

extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var image: UIImage?
        if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            image = originalImage
            
        } else if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            image = editedImage
        }
        UIPasteboard.general.image = image
        customInputBar.inputTextView.paste(self)
        
        dismiss(animated: true, completion: nil)
               
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }
    }
}

