//
//  SignupController.swift
//  ChatExample
//
//  Created by Michael Aidoo on 2018-07-04.
//  Copyright © 2018 Michael Aidoo. All rights reserved.
//

import Foundation
import UIKit

class SignupController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(didTapImageView)))
    }
    
    private func showPhotoLibrary() {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = true
        self.present(pickerController, animated: true, completion: nil)
    }
    
    @objc func didTapImageView() {
        showPhotoLibrary()
    }
    
    @IBAction func signupButtonTapped(_ sender: Any) {
        let image = imageView.image ?? UIImage(named: "placeholder")!
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        FirebaseDatabaseManager.shared.createUser(email, password: password, profileImage: image) { success in
            if success {
                self.performSegue(withIdentifier: "ShowChatFromSign", sender: nil)
            }
        }
    }
}

extension SignupController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var image: UIImage?
        if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            image = originalImage
            
        } else if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            image = editedImage
        }
        self.image = image
        dismiss(animated: true, completion: nil)
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }
    }
}
